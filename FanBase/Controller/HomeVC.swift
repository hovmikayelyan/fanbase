//
//  HomeVC.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/28/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import UIKit

class HomeVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var movieTable: UITableView!
    @IBOutlet weak var actorsCollection: UICollectionView!
    
    private(set) public var actors = [Actor]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieTable.dataSource = self
        movieTable.delegate = self
        
        actorsCollection.dataSource = self
        actorsCollection.delegate = self
    }

    func initActors(movie: Movie){
        actors = DataService.instance.getActors(forMovieName: movie.title)
//        navigationItem.title = category.title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getMovies().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell") as? MovieCell{
            let movie = DataService.instance.getMovies()[indexPath.row]
            cell.updateViews(movie: movie)
            return cell
        }
        else{
            return MovieCell()
        }
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let movie = DataService.instance.getMovies()()[indexPath.row]
//        performSegue(withIdentifier: "ProductsVC", sender: category)
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return actors.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActorsCell", for: indexPath) as? ActorsCell {
            let actor = actors[indexPath.row]
            cell.updateViews(actor: actor)
            return cell
        }
        return ActorsCell()
    }
}

