//
//  Actor.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/29/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import Foundation

struct Actor {
    private(set) public var fullName: String
    private(set) public var imageName: String
    private(set) public var age: Int
    private(set) public var info: String
    private(set) public var fromMovie: String

    init(fullName: String, imageName: String, age: Int, info: String, fromMovie: String) {
        self.fullName = fullName;
        self.imageName = imageName;
        self.age = age;
        self.info = info;
        self.fromMovie = fromMovie;
    }
    
}
