//
//  Movie.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/29/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import Foundation

struct Movie {
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title;
        self.imageName = imageName;
    }
}
