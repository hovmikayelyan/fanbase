//
//  DataService.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/29/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    
    private let movies = [
        Movie(title: "The Magicians", imageName: "magicians.jpg"),
        Movie(title: "The Vampire Diaries", imageName: "diaries.jpg"),
        Movie(title: "Wizards of Waverly Place", imageName: "waverly.jpg"),
        Movie(title: "Once Upon A Time", imageName: "ouat.jpg")
    ];
    
    private let actors = [
        Actor(fullName: "Hale Appleman", imageName: "hale.jpg", age: 36, info: "American actor", fromMovie: "The Magicians"),
        Actor(fullName: "Jade Tailor", imageName: "jade.jpg", age: 36, info: "American film actress", fromMovie: "The Magicians"),
        Actor(fullName: "Olivia Taylor Dudley", imageName: "olivia.jpg", age: 36, info: "American actress", fromMovie: "The Magicians"),
        Actor(fullName: "Summer Bishil", imageName: "summer.jpg", age: 33, info: "American actress", fromMovie: "The Magicians"),
        
        Actor(fullName: "Ian Somerhalder", imageName: "ian.jpg", age: 43, info: "American actor", fromMovie: "The Vampire Diaries"),
        Actor(fullName: "Paul Wesley", imageName: "paul.jpg", age: 39, info: "American actor", fromMovie: "The Vampire Diaries"),
        Actor(fullName: "Nina Dobrev", imageName: "nina.jpg", age: 33, info: "Canadian actress", fromMovie: "The Vampire Diaries"),
        Actor(fullName: "Candiece King", imageName: "candiece.jpg", age: 35, info: "American actress", fromMovie: "The Vampire Diaries"),
        Actor(fullName: "Kat Graham", imageName: "katerina.jpg", age: 32, info: "American actress", fromMovie: "The Vampire Diaries"),
        Actor(fullName: "Joseph Morgan", imageName: "joseph.jpg", age: 41, info: "British actor", fromMovie: "The Vampire Diaries"),
        
        Actor(fullName: "Selena Gomez", imageName: "selena_gomez.jpg", age: 29, info: "American singer", fromMovie: "Wizards of Waverly Place"),
        Actor(fullName: "Jennifer Stone", imageName: "jen_stone.jpg", age: 29, info: "American actress", fromMovie: "Wizards of Waverly Place"),
        Actor(fullName: "David Henrie", imageName: "david_henrie.jpg", age: 32, info: "American actor", fromMovie: "Wizards of Waverly Place"),
        
        Actor(fullName: "Lana Parilla", imageName: "lana_parilla.png", age: 44, info: "American actress", fromMovie: "Once Upon A Time"),
        Actor(fullName: "Jamie Dornan", imageName: "jamie_dornan.jpg", age: 40, info: "American actor", fromMovie: "Once Upon A Time"),
        Actor(fullName: "Rebecca Mader", imageName: "rebecca.jpg", age: 45, info: "American actor", fromMovie: "Once Upon A Time"),
    ];
    
    
    func getMovies() -> [Movie] {
        return movies;
    }
    
    func getActors(forMovieName title: String) -> [Actor] {
        switch title {
        case "Once Upon A Time":
            return getActorsFromSpecificMovie(title: "Once Upon A Time")
        case "Wizards of Waverly Place":
              return getActorsFromSpecificMovie(title: "Wizards of Waverly Place")
        case "The Vampire Diaries":
             return getActorsFromSpecificMovie(title: "The Vampire Diaries")
        case "The Magicians":
             return getActorsFromSpecificMovie(title: "The Magicians")
        default:
              return getActorsFromSpecificMovie(title: "The Vampire Diaries")
        }
    }
    
    func getActorsFromSpecificMovie(title: String) -> [Actor] {
        var product = [Actor]()
        for item in actors {
            if(item.fromMovie == title){
                product.append(item)
            }
        }
        
        return product;
    }
    
    
}
