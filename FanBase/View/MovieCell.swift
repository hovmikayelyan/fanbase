//
//  MovieCell.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/29/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieName: UILabel!
    
    func updateViews(movie: Movie){
        movieImage.image = UIImage(named: movie.imageName);
        movieName.text = movie.title;
    }
    
}
