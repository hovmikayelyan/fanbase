//
//  ActorsCell.swift
//  FanBase
//
//  Created by Hovhannes Mikayelyan on 5/29/22.
//  Copyright © 2022 Hovhannes Mikayelyan. All rights reserved.
//

import UIKit

class ActorsCell: UICollectionViewCell {
    
    @IBOutlet weak var actorImage: UIImageView!
    @IBOutlet weak var actorName: UILabel!
    
    func updateViews(actor: Actor){
        actorImage.image = UIImage(named: actor.imageName)
        actorName.text = actor.fullName
    }
}
